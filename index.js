const path = require("path");
const http = require('http');
const puppeteer = require('puppeteer');
const chalk = require("chalk");
const fs = require('fs');
const csv = require('@fast-csv/format');
const csvParse = require('@fast-csv/parse');

const error = chalk.bold.red;
const success = chalk.keyword("green");

class CsvFile {
    static write(filestream, rows, options) {
        return new Promise((res, rej) => {
            csv.writeToStream(filestream, rows, options)
                .on('error', err => rej(err))
                .on('finish', () => res());
        });
    }

    constructor(opts) {
        this.headers = opts.headers;
        this.path = opts.path;
        this.writeOpts = { headers: this.headers, includeEndRowDelimiter: true, writeHeaders: true  };
    }

    create(rows) {
        return CsvFile.write(fs.createWriteStream(this.path), rows, { ...this.writeOpts });
    }

    append(rows) {
        return CsvFile.write(fs.createWriteStream(this.path, { flags: 'a' }), rows, {
            ...this.writeOpts,
            writeHeaders: false,
        });
    }
}

(async () => {
    const csvFile = new CsvFile({
        path: path.resolve(__dirname, 'append.csv'),
        headers: ['index', 'name', 'date_of_birth', 'age', 'place_of_birth', 'nation', 'youth_team', ' position', 'height', 'weight', 'shoots', 'contract'],
    });
    const rows = [];
    let lastRow = 0;
    if(fs.existsSync('./append.csv'))
        await fs.createReadStream(path.resolve(__dirname, 'append.csv'))
            .pipe(csvParse.parse())
            .on('error', error => console.error(error))
            .on('data', row => rows.push(row))
            .on('end', rowCount => lastRow = rowCount);
    const browser = await puppeteer.launch({ headless: true });
    const page = await browser.newPage();
    for (let i = lastRow + 1; i < 20000; i++) {
        try {
            console.log(`https://www.eliteprospects.com/player/${i}/jack-hughes`);
            await page.goto(`https://www.eliteprospects.com/player/${i}/jack-hughes`, { waitUntil: 'networkidle0'});
            await page.waitForSelector('.plytitle', {
                timeout: 1000
            })
                .catch(e => {
                    console.log(e);
                });
            const name = await page.$eval('.plytitle', node => [...node.childNodes].filter(e => e.nodeType === 3).map(e => e.textContent).map(t => t.trim()).filter(e => e !== '')[0]);
            await page.waitForSelector('div.table-view', {
                timeout: 1000
            })
                .catch(e => {
                    console.log(e);
                });

            const player_info = await page.$eval('.table-view', node => {
                const keys = [...node.querySelectorAll('.table-view div.row ul li div:first-child')].map(item => item.textContent.trim()).map(key => key.trim().replace(/\s+/g, '_').toLowerCase());
                const values = [...node.querySelectorAll('.table-view div.row ul li div:nth-child(2)')].map((item, index) => {
                    return keys[index] === 'nation' ?
                        item.textContent.split('/').map(e => e.trim()).join(' / ')
                        :
                        item.textContent.trim()
                });
                return {keys, values}
            });
            const formattedPlayerInfo = player_info.keys.reduce((o, k, i) => ({...o, [k]: player_info.values[i]}), {});

            if(fs.existsSync(path.resolve(__dirname, 'append.csv'))) {
                await csvFile.append([{
                    index: i,
                    name: name,
                    ...formattedPlayerInfo
                }])
            } else {
                await csvFile.create([{
                    index: i,
                    name: name,
                    ...formattedPlayerInfo
                }]);
            }
        } catch (e) {
            console.log('oshibka blyat\'')
        }

    }
})();

const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    res.end('Hello, World!\n');
});

server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
});