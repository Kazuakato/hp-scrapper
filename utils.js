const nameRegex = /^(<[\w]+>)(\r\n|\n|\r|.)+(<\/[\w]+>)/gm;

exports.nameRegex = nameRegex;